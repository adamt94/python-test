# Setup
To run the application 
run ./main.py (runs the api)
run ./backend_service.py (this calls the api with a list)

# Urls
Swagger documentation at [localhost:5000/api/ui]()

# Packages

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install packages.

```bash
pip install flask
pip install pytest
pip install connexion
```

