#!/usr/bin/env python3
import requests 
import json
# api-endpoint 
API_ENDPOINT = "http://localhost:5000/api/total"


headers={"Content-Type":"application/json"}


def retreive_numbers():
    return list(range(10001))

#data for request
data = str(retreive_numbers())

# sending get request and saving the response as response object 
request = requests.post(url = API_ENDPOINT, headers=headers, data = data) 

print("The pastebin URL is:%s"%request.text) 