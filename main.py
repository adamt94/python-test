#!/usr/bin/env python3
from flask import Flask,jsonify
import pytest
import connexion

#Assumptions:
# seperate backend service calls the api
# using post so can enter long array in body rather then in url

#Run main.py then run backend_service.py which calls the endpoint
#Swagger documentation at localhost:5000/api/ui

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

#setup swagger  localhost:5000/api/ui
app.add_api('swagger.yml')




#URL route of the application
@app.route('/')
def home():
    """
    localhost:5000/
    """
    return  str("Test")



# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(debug=True)