from flask import Flask,jsonify,request
import json


def calculate_total(numbers_to_add):
    total = 0
    for x in numbers_to_add:
        if isinstance(x, int):
            total += x
    return total


   



#GET Request localhost:5000/total
def read():
    
    data  = json.loads(str(request.json))
    total =  calculate_total(data)
    return jsonify(total = total)