#!/usr/bin/env python

import pytest

from flask import Flask
from total import calculate_total

app = Flask(__name__)

def test_calculate_total():
    assert calculate_total([1,2]) == 3
    assert calculate_total(["test"]) == 0

